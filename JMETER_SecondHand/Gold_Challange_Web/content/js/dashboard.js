/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 98.91666666666667, "KoPercent": 1.0833333333333333};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.5713888888888888, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.655, 500, 1500, "POST /products.json (for buy)"], "isController": false}, {"data": [0.655, 500, 1500, "GET /products.json"], "isController": false}, {"data": [0.275, 500, 1500, "POST Register"], "isController": false}, {"data": [0.72, 500, 1500, "GET OFFERS"], "isController": false}, {"data": [0.7175, 500, 1500, "POST /products.json"], "isController": false}, {"data": [0.5075, 500, 1500, "POST /users/sign_in.json"], "isController": false}, {"data": [0.375, 500, 1500, "POST Register akun"], "isController": false}, {"data": [0.76, 500, 1500, "DELETE /products/{id}.json"], "isController": false}, {"data": [0.7875, 500, 1500, "GET /categories.json"], "isController": false}, {"data": [0.76, 500, 1500, "GET /products/{id}.json "], "isController": false}, {"data": [0.625, 500, 1500, "POST /users/sign_in.json Buyer"], "isController": false}, {"data": [0.4875, 500, 1500, "POST /offers.json"], "isController": false}, {"data": [0.675, 500, 1500, "PUT /products/{id}.json"], "isController": false}, {"data": [0.13, 500, 1500, "PUT /profiles.json"], "isController": false}, {"data": [0.60125, 500, 1500, "GET /profiles.json "], "isController": false}, {"data": [0.8225, 500, 1500, "GET /categories/{id}.json"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3600, 39, 1.0833333333333333, 889.6677777777776, 24, 3837, 765.0, 1674.5000000000005, 1955.0, 2698.6999999999935, 20.690844301396634, 45.66578336937467, 91.05349984194494], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["POST /products.json (for buy)", 200, 0, 0.0, 693.0900000000003, 93, 2096, 602.0, 1203.6000000000001, 1298.95, 1879.790000000002, 1.2406870925118332, 2.3047943638687105, 14.30261414708966], "isController": false}, {"data": ["GET /products.json", 200, 0, 0.0, 632.2700000000003, 135, 1714, 584.5, 984.0, 1116.8999999999996, 1490.4000000000005, 1.2308980016370943, 16.14501233398263, 0.8474564454435233], "isController": false}, {"data": ["POST Register", 200, 12, 6.0, 1488.9950000000006, 610, 2936, 1432.5, 2027.5000000000002, 2316.7499999999995, 2816.92, 1.2056472517270898, 1.986150786534126, 1.2855449299518946], "isController": false}, {"data": ["GET OFFERS", 200, 0, 0.0, 647.3499999999997, 32, 1639, 552.5, 1158.7, 1339.7499999999998, 1622.6700000000003, 1.2228751016514927, 1.4893878325455978, 0.8534808375471571], "isController": false}, {"data": ["POST /products.json", 200, 0, 0.0, 597.045, 148, 1494, 535.0, 978.8000000000001, 1189.85, 1391.2900000000006, 1.2315574275228454, 2.2823260733022983, 14.194000025400873], "isController": false}, {"data": ["POST /users/sign_in.json", 200, 1, 0.5, 984.4249999999995, 179, 2931, 893.5, 1539.4, 2171.5999999999995, 2625.0, 1.2068767839147463, 2.080984402249015, 0.9675993184163362], "isController": false}, {"data": ["POST Register akun", 200, 9, 4.5, 1240.9500000000005, 356, 2525, 1212.0, 1801.9, 1981.6499999999999, 2193.370000000001, 1.2391420180666906, 2.054936732119181, 1.6143939124050508], "isController": false}, {"data": ["DELETE /products/{id}.json", 200, 0, 0.0, 535.3749999999999, 56, 1470, 474.0, 941.1000000000001, 1145.9499999999996, 1447.7800000000002, 1.238627848069908, 1.2083032385473373, 0.9965148108924934], "isController": false}, {"data": ["GET /categories.json", 200, 0, 0.0, 541.2750000000001, 31, 1758, 460.5, 1095.1000000000001, 1223.6999999999994, 1482.0, 1.231193518997316, 1.2865010403585238, 0.8502569539348945], "isController": false}, {"data": ["GET /products/{id}.json ", 200, 0, 0.0, 551.7850000000005, 91, 1635, 480.0, 890.0, 1216.9999999999998, 1551.1400000000008, 1.2341717473403597, 2.2075102146996026, 0.85773731194308], "isController": false}, {"data": ["POST /users/sign_in.json Buyer", 200, 0, 0.0, 762.295, 25, 2222, 724.5, 1244.9, 1365.9, 1970.6200000000003, 1.2443460028495523, 2.1420675178252564, 1.2333182294356269], "isController": false}, {"data": ["POST /offers.json", 200, 0, 0.0, 1019.3100000000005, 89, 2080, 1006.0, 1550.0, 1788.3999999999999, 2063.99, 1.2643184059473538, 3.509786170174097, 14.568250821411866], "isController": false}, {"data": ["PUT /products/{id}.json", 200, 0, 0.0, 625.0399999999998, 130, 1541, 561.5, 1106.0, 1216.8, 1499.93, 1.2360481069923241, 2.2798388289834737, 14.464714158081282], "isController": false}, {"data": ["PUT /profiles.json", 400, 17, 4.25, 1803.8724999999997, 906, 3837, 1733.5, 2350.6000000000004, 2833.1499999999996, 3413.2000000000007, 2.3483549773383743, 3.56333628039652, 26.906093687621823], "isController": false}, {"data": ["GET /profiles.json ", 400, 0, 0.0, 806.8799999999992, 34, 2381, 763.0, 1314.6000000000001, 1554.6, 2138.5800000000004, 2.377612401626287, 3.273959506660287, 1.887665197624171], "isController": false}, {"data": ["GET /categories/{id}.json", 200, 0, 0.0, 473.31, 24, 1364, 377.0, 983.8000000000001, 1066.8, 1333.5000000000005, 1.2319443161169115, 0.8770384828605747, 0.8531815924420216], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["The operation lasted too long: It took 2,941 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 2,926 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 3,334 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 2,991 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 3,833 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 3,150 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 3,053 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 3,021 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 3,093 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 2,951 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 2,936 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 3,837 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 2,964 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 2,960 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 3,149 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 3,167 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 3,674 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["401/Unauthorized", 20, 51.282051282051285, 0.5555555555555556], "isController": false}, {"data": ["The operation lasted too long: It took 2,931 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}, {"data": ["The operation lasted too long: It took 3,414 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.5641025641025643, 0.027777777777777776], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3600, 39, "401/Unauthorized", 20, "The operation lasted too long: It took 2,941 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 2,926 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,334 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 2,991 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["POST Register", 200, 12, "401/Unauthorized", 11, "The operation lasted too long: It took 2,936 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["POST /users/sign_in.json", 200, 1, "The operation lasted too long: It took 2,931 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST Register akun", 200, 9, "401/Unauthorized", 9, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["PUT /profiles.json", 400, 17, "The operation lasted too long: It took 2,941 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 2,926 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,334 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 2,991 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,833 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
