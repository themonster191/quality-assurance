/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 98.4, "KoPercent": 1.6};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.4365, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.6075, 500, 1500, "List Product by id "], "isController": false}, {"data": [0.6625, 500, 1500, "Delete Product"], "isController": false}, {"data": [0.2175, 500, 1500, "PUT_buyer_orderbyID"], "isController": false}, {"data": [0.63, 500, 1500, "View_Product_byID"], "isController": false}, {"data": [0.185, 500, 1500, "get_buyer_offers"], "isController": false}, {"data": [0.1925, 500, 1500, "get_buyer_orderbyID"], "isController": false}, {"data": [0.505, 500, 1500, "List Product "], "isController": false}, {"data": [0.43125, 500, 1500, "Post Register "], "isController": false}, {"data": [0.6325, 500, 1500, "Post Sign (Login) "], "isController": false}, {"data": [0.6, 500, 1500, "Post Sign (Login) buyer"], "isController": false}, {"data": [0.1775, 500, 1500, "Post Buyer_create_offers"], "isController": false}, {"data": [0.3375, 500, 1500, "Create Product"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3000, 48, 1.6, 1256.7200000000023, 24, 31705, 1022.0, 2042.7000000000003, 2355.749999999999, 4319.279999999984, 14.502492011543984, 1036.2614796288328, 77.44700849846032], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["List Product by id ", 200, 0, 0.0, 733.905, 102, 2027, 666.0, 1177.9, 1393.0, 1953.94, 1.034414986604326, 0.7141908159465414, 0.4220039382118068], "isController": false}, {"data": ["Delete Product", 200, 0, 0.0, 656.2099999999999, 136, 2157, 557.5, 1138.7, 1357.0, 2019.0600000000027, 1.0350252546162126, 0.7211164477803883, 0.4252852060605904], "isController": false}, {"data": ["PUT_buyer_orderbyID", 200, 6, 3.0, 1697.7000000000007, 53, 3449, 1763.5, 2464.6, 2600.65, 3401.2300000000014, 1.054179558404183, 0.6887169185277329, 0.9089930905118568], "isController": false}, {"data": ["View_Product_byID", 200, 0, 0.0, 698.7550000000003, 24, 1598, 712.5, 1121.9, 1224.55, 1583.2700000000007, 1.0511875791675644, 0.9052942405432538, 0.43060708382169754], "isController": false}, {"data": ["get_buyer_offers", 200, 10, 5.0, 2075.549999999999, 586, 17598, 1737.0, 2583.3, 2999.5499999999975, 16310.900000000034, 1.045308914917081, 554.9691566496674, 0.594999225818085], "isController": false}, {"data": ["get_buyer_orderbyID", 200, 23, 11.5, 3073.6199999999994, 510, 31705, 1639.5, 5539.800000000004, 19150.05, 22660.160000000018, 1.045352623573747, 557.3530930432436, 0.6042117747212828], "isController": false}, {"data": ["List Product ", 200, 0, 0.0, 954.8399999999997, 100, 2222, 895.5, 1569.6000000000001, 1738.5499999999997, 2059.3600000000006, 1.0357756924160504, 0.2735550362650964, 0.41729925857881217], "isController": false}, {"data": ["Post Register ", 400, 3, 0.75, 1110.2300000000002, 187, 2781, 1068.5, 1791.0, 1967.5, 2322.2200000000007, 1.997842330283294, 1.1422673278484237, 2.2246091408778517], "isController": false}, {"data": ["Post Sign (Login) ", 200, 0, 0.0, 705.3000000000001, 165, 1894, 631.5, 1187.0, 1450.7499999999995, 1849.3800000000006, 1.0392363691159734, 0.5102356257372083, 0.825238926936487], "isController": false}, {"data": ["Post Sign (Login) buyer", 400, 0, 0.0, 767.7150000000001, 140, 1989, 752.0, 1198.9, 1315.55, 1469.92, 2.0857341001882377, 1.0062037553642473, 1.0132716238482837], "isController": false}, {"data": ["Post Buyer_create_offers", 200, 4, 2.0, 1791.8550000000007, 67, 4320, 1783.0, 2666.9, 2730.6, 4244.170000000004, 1.0624903711810112, 0.6951841295813257, 0.8806219021765116], "isController": false}, {"data": ["Create Product", 400, 2, 0.5, 1353.587499999999, 147, 3292, 1287.5, 1977.2000000000012, 2444.8999999999996, 2880.5000000000014, 2.053535675048515, 1.3396111630199297, 73.55910681305382], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["The operation lasted too long: It took 3,909 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,775 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 14,345 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,157 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 9,309 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 5,317 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 11,601 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 19,153 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 16,349 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 19,718 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 12,539 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 20,136 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["400/Bad Request", 3, 6.25, 0.1], "isController": false}, {"data": ["The operation lasted too long: It took 8,470 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 20,575 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 6,017 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 4,926 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 5,608 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 17,598 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 9,584 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 6,769 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,403 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 20,795 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 4,248 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,282 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 12,094 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,155 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 22,679 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,048 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,189 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 5,285 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 19,094 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 31,705 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,010 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,865 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,292 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 19,515 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 20,203 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 4,320 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 20,549 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,014 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 9,319 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 9,370 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 9,862 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,449 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 3,226 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, 2.0833333333333335, 0.03333333333333333], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3000, 48, "400/Bad Request", 3, "The operation lasted too long: It took 3,909 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,775 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 14,345 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,157 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["PUT_buyer_orderbyID", 200, 6, "The operation lasted too long: It took 3,157 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,155 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,189 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,449 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,403 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1], "isController": false}, {"data": [], "isController": false}, {"data": ["get_buyer_offers", 200, 10, "The operation lasted too long: It took 3,010 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 12,094 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 5,317 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 11,601 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,048 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1], "isController": false}, {"data": ["get_buyer_orderbyID", 200, 23, "The operation lasted too long: It took 3,909 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 14,345 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 9,309 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 19,153 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 22,679 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1], "isController": false}, {"data": [], "isController": false}, {"data": ["Post Register ", 400, 3, "400/Bad Request", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Post Buyer_create_offers", 200, 4, "The operation lasted too long: It took 3,865 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,775 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 4,320 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 4,248 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "", ""], "isController": false}, {"data": ["Create Product", 400, 2, "The operation lasted too long: It took 3,292 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "The operation lasted too long: It took 3,282 milliseconds, but should not have lasted longer than 2,900 milliseconds.", 1, "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
