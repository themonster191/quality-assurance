
# SecondHand Android Automation Testing

This Repository contain automation testing for SecondHand Mobile (Android) application using Katalon Studio as part of [BINAR Academy](https://www.binaracademy.com/) Quality Assurance Engineer Bootcamp. In this project, you can automatically do feature exploration with proper configuration.


## Project Setup

Clone this project and open the project using Katalon Studio. 

Make sure to already had Appium installed (preferably version 1.22.3) for running the test. Run this npm install to get specific version

```bash
  npm install -g appium@1.22.3
```
    
To get demo application used in this project, download the APK from [this link](https://drive.google.com/file/d/1CwFIat5Ok37pLBOYCJjeCTzEEsqx6B9V/view)

## Test Case

Test Case used for this project can be seen on this [spreadsheet](https://docs.google.com/spreadsheets/d/1rL2e9HoH8mp0NDpeOUfxeUi8w5OeSMB9hNEY5Vb8hxY)
## Tested Features

- Authentication
  - Register
  - Login
  - Logout
- Categories
  - Search by Text
  - Search by Categories
- Profile
  - View Profile
  - Edit Profile
- Product
  - Add new Product
  - View added Product
  - Edit added Product
  - Delete added Product
- Offer
  - Buyer sending Offer
  - Seller responding to Offer



## Project Status

This project is done. There'll be no more improvement made. 
## Lessons Learned

Since this project is built in limited time, there'll be many mistakes. Common mistakes you'll encounter in this project are misconfigured object in step definition, Objects in the Object Repository needs re-adjustment for different sceen size, inconsistent filename capitalizing, and many more.

